//
//  AppController.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/15.
//

import Foundation
import UIKit

class AppController {
    static let shared = AppController()
    
    var window: UIWindow?
    
    func start() {
//        let storyboard = UIStoryboard(name: "Home", bundle: Bundle.main)
//        let vc = storyboard.instantiateViewController(withIdentifier: "HomeTabBarController") as! HomeTabBarController
//        let nav = UINavigationController(rootViewController: vc)
//        window?.rootViewController = nav
        
        window?.rootViewController = makeAppTabBarController()
    }
    
    private func makeAppTabBarController() -> UITabBarController {
        let tabBarController = UITabBarController()
        tabBarController.viewControllers = [
            GamesBranch(),
            TeamsBranch()
        ]
        tabBarController.tabBar.backgroundColor = .white
        return tabBarController
    }
//
    private func GamesBranch() -> UINavigationController {
//        let nav = UINavigationController()
//        nav.tabBarItem = UITabBarItem(title: "比賽", image: nil, tag: 0)
//        let coordinator = TransferMainCoordinator(navigationController: nav, parent: nil)
//        self.topCoordinator = coordinator
//        nav.viewControllers = [ coordinator.start() ]
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "GamesViewController") as! GamesViewController
        
        let nav = UINavigationController(rootViewController: vc)
        nav.tabBarItem = UITabBarItem(title: "比賽", image: nil, tag: 0)
        return nav
    }

    private func TeamsBranch() -> UINavigationController {
//        let nav = UINavigationController()
//        nav.tabBarItem = UITabBarItem(title: "隊伍", image: nil, tag: 1)
//        let coordinator = SettingMainCoordinator(navigationController: nav, parent: nil)
//        self.topCoordinator = coordinator
//        nav.viewControllers = [ coordinator.start() ]
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "TeamsViewController") as! TeamsViewController
        
        let nav = UINavigationController(rootViewController: vc)
        nav.tabBarItem = UITabBarItem(title: "隊伍", image: nil, tag: 1)
        return nav
    }

//    private func SwitchToGames(){
//        let storyboard = UIStoryboard(name: "Games", bundle: Bundle.main)
//        let vc = storyboard.instantiateViewController(withIdentifier: "GamesViewController") as! GamesViewController
//        navigationController?.pushViewController(vc, animated: true)
//    }
}
