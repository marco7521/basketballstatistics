//
//  Coordinator.swift
//  Coordinator
//
//  Created by admin on 2021/5/31.
//

import Foundation
import UIKit

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] {get set}
    var navigationController: UINavigationController {get set}
    
    func start()
}

