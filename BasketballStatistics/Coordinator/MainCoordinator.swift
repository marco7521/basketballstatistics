//
//  MainCoordinator.swift
//  Coordinator
//
//  Created by admin on 2021/5/31.
//

import Foundation
import UIKit

class MainCoordinator: Coordinator{
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
//        let vc = GamesViewController.instantiate()
//        vc.coordinator = self
//        navigationController.pushViewController(vc, animated: false)
    }
    
    func toTeamsStart() {
//        let child = TeamsCoordinator(navigationController: navigationController)
//        child.parentCoordinator = self
//        childCoordinators.append(child)
//        child.start()
        let vc = TeamsViewController.instantiate()
//        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }

}
