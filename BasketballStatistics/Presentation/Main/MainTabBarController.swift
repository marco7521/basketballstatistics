//
//  MainTabBarController.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/18.
//

import UIKit

class MainTabBarController: UITabBarController {

    let games = GamesCoordinator(navigationController: UINavigationController())
    let teams = TeamsCoordinator(navigationController: UINavigationController())

    override func viewDidLoad() {
        super.viewDidLoad()

        games.start()
        teams.start()
        viewControllers = [games.navigationController, teams.navigationController]
    }
}
