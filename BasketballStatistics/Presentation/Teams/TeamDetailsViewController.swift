//
//  TeamDetailsViewController.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/23.
//

import UIKit
import CoreData

class TeamDetailsViewController: UIViewController, Storyboarded {

    @IBOutlet weak var teamImageView: UIImageView!
    @IBOutlet weak var teamNameLabel: UILabel!
    @IBOutlet weak var playersCountLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addBtn: UIButton!
    
    var team: Teams?
    var players: [Players]?
    var coordinator: TeamDetailsCoordinator?

    private var appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchPlayers()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        coordinator?.didFinish()
    }
    
    @IBAction func addPlayerAction(_ sender: Any) {
        showAddPlayerAlert(teamId: team?.id ?? "", players: players ?? []) {
            self.fetchPlayers()
        }
    }
}


extension TeamDetailsViewController{
    private func initUI(){
        self.title = "球隊資訊"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "編輯", style: .plain, target: self, action: #selector(editTeam))
        
        teamNameLabel.text = team?.name
        teamImageView.image = UIImage(named: "unknown")
        if let iconName = team?.iconName{
            teamImageView.image = UIImage(named: iconName)
        }

        //設定Top鍵陰影
        addBtn.layer.masksToBounds = false
        addBtn.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) //陰影顏色
        addBtn.layer.shadowOffset = CGSize(width: 0, height: 2) //陰影偏移量
        addBtn.layer.shadowOpacity = 0.3 //陰影透明度
        addBtn.layer.shadowRadius = 10 //陰影模糊計算的半徑
    }
    
    @objc public func editTeam() {
        if let team = team{
            coordinator?.toAddTeam(team: team)
        }
    }
    
    private func fetchPlayers() {
        let request = NSFetchRequest<Players>(entityName: "Players" )
        do {
            let results = try context.fetch(request)
            let data = results.filter({$0.teamId == self.team?.id})
            players = data
            playersCountLabel.text = "\(data.count)名隊員"
            tableView.reloadData()
        }catch{
            showCustomAlert(mode: .One, message: "\(error)")
        }
    }
}


extension TeamDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamDetailsTableViewCell", for: indexPath) as! TeamDetailsTableViewCell
        let playerName = players?[indexPath.row].name
        cell.nameLabel.text = playerName
        
        let playerNumber = players?[indexPath.row].number
        cell.numberLabel.text = "背號 \(playerNumber ?? "")"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let player = players?[indexPath.row]{
            showEditPlayerAlert(editPlayer: player, teamId: team?.id ?? "", players: players ?? []) {
                self.fetchPlayers()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //Update memory
            let data = players?.remove(at: indexPath.row)
            //update database
            deletePlayer(id: data?.id ?? "")
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    //MARK: 刪除
    private func deletePlayer(id: String) {
        let request = NSFetchRequest<Players>(entityName: "Players" )
        // 搜尋到符合要刪除的球員name
        request.predicate = NSPredicate(format: "id = %@ AND teamId = %@", argumentArray: [id, team?.id ?? ""])
        
        do {            
            let players = try context.fetch(request)
            for player in players {
                // 刪除player
                context.delete(player)
            }

            // 刪除後記得儲存Context
            appDelegate.saveContext()
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}
