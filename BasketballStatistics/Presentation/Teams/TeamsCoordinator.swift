//
//  TeamsCoordinator.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/17.
//

import Foundation
import UIKit

class TeamsCoordinator: Coordinator {
    
    weak var parentCoordinator: MainCoordinator?
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = TeamsViewController.instantiate()
        vc.coordinator = self
        vc.tabBarItem = UITabBarItem(title: "球隊", image: UIImage(named: "Teams"), tag: 1)
//        vc.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 1)
        navigationController.pushViewController(vc, animated: true)
    }
    
    func toAddTeam(){
        let child = AddTeamCoordinator(navigationController: navigationController)
        child.parentCoordinator = self
        childCoordinators.append(child)
        child.start()
//        let vc = AddTeamViewController.instantiate()
//        vc.coordinator = self
//        navigationController.pushViewController(vc, animated: true)
    }
    
    func childDidFinish(_ child: Coordinator?){
        for (index, coordinator) in
            childCoordinators.enumerated() {
            if coordinator === child{
                childCoordinators.remove(at: index)
                break
            }
        }
    }
    
    func toTeamDetail(teamData: Teams){
        let child = TeamDetailsCoordinator(navigationController: navigationController)
        child.parentCoordinator = self
        childCoordinators.append(child)
        child.start(data: teamData)
    }
}
