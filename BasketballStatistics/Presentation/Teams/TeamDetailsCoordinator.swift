//
//  TeamDetailsCoordinator.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/23.
//

import Foundation
import UIKit

class TeamDetailsCoordinator: Coordinator {
    
    weak var parentCoordinator: TeamsCoordinator?
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = TeamDetailsViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func start(data: Teams) {
        let vc = TeamDetailsViewController.instantiate()
        vc.coordinator = self
        vc.team = data
        navigationController.pushViewController(vc, animated: true)
    }
    
    func toAddTeam(team: Teams){
        let child = AddTeamCoordinator(navigationController: navigationController)
        child.parentCoordinator = TeamsCoordinator(navigationController: UINavigationController())
        childCoordinators.append(child)
        child.start(isAdd: false, team: team)
    }

    func didFinish(){
        parentCoordinator?.childDidFinish(self)
    }
}
