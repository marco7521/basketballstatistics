//
//  TeamsViewController.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/15.
//

import UIKit
import CoreData

class TeamsViewController: UIViewController, Storyboarded  {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var emptyView: UIView!
    
    var coordinator: TeamsCoordinator?
    
    var teams: [Teams]?
    var players: [Players]?

    private var appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        initUI()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchTeams()
        fetchPlayers()
    }
    
    private func fetchTeams() {
        let request = NSFetchRequest<Teams>(entityName: "Teams" )
        do {
            let results = try context.fetch(request)
            teams = results
            tableView.reloadData()
        }catch{
            showCustomAlert(mode: .One, message: "\(error)")
        }
        setupTableView()
    }
    
    private func fetchPlayers() {
        let request = NSFetchRequest<Players>(entityName: "Players" )
        do {
            let results = try context.fetch(request)
            players = results
            tableView.reloadData()
        }catch{
            showCustomAlert(mode: .One, message: "\(error)")
        }
    }
}

extension TeamsViewController{
    private func initUI(){
        self.title = "球隊"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "新增球隊", style: .plain, target: self, action: #selector(addTeam))
    }
    
    @objc public func addTeam() {
        coordinator?.toAddTeam()
    }
}

extension TeamsViewController: UITableViewDelegate, UITableViewDataSource{
    private func setupTableView(){
        tableView.backgroundView = emptyView
        if let teams = teams{
            if !teams.isEmpty{
                tableView.backgroundView = nil
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamsTableViewCell", for: indexPath) as! TeamsTableViewCell
        
        cell.teamIcon.image = UIImage(named: "unknown") //未設定圖片就使用預設圖片
        
        let teamName = teams?[indexPath.row].name
        let iconName = teams?[indexPath.row].iconName ?? ""
        cell.teamNameLabel.text = teamName
        if !iconName.isEmpty{
            cell.teamIcon.image = UIImage(named: iconName)
        }
        
        let players = players?.filter({$0.teamId == teams?[indexPath.row].id})
        cell.playersCountLabel.text = "\(players?.count ?? 0)名隊員"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let team = teams?[indexPath.row] {
            coordinator?.toTeamDetail(teamData: team)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            showCustomAlert(mode: .Two, message: "比賽紀錄中，若有含此要刪除的球隊，比賽紀錄也一併刪除，是否繼續？", yesBtnAction: { [self] in
                //Update memory
                let data = teams?.remove(at: indexPath.row)
                //update database
                deleteTeam(id: data?.id ?? "")
                tableView.deleteRows(at: [indexPath], with: .fade)
            })
        }
    }
    
    //MARK: 刪除
    private func deleteTeam(id: String) {
        let request_team = NSFetchRequest<Teams>(entityName: "Teams" )
        // 透過 predicate 將team找出來
        request_team.predicate = NSPredicate(format: "id = %@", id)
        
        let request_player = NSFetchRequest<Players>(entityName: "Players" )
        // 將屬於這team裡的player找出來(player也要刪除)
        request_player.predicate = NSPredicate(format: "teamId = %@", id)

        let request_game = NSFetchRequest<Games>(entityName: "Games" )
        // Game裡有紀錄這隊的話也一併刪除
        request_game.predicate = NSPredicate(format: "awayTeamId = %@ || homeTeamId = %@", argumentArray: [id, id])

        do {
            let teams = try context.fetch(request_team)
            for team in teams {
                // 刪除team
                context.delete(team)
            }
            
            let players = try context.fetch(request_player)
            for player in players {
                // 刪除player
                context.delete(player)
            }
            
            let games = try context.fetch(request_game)
            for game in games {
                // 刪除player
                context.delete(game)
            }

            // 刪除後記得儲存Context
            appDelegate.saveContext()
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}
