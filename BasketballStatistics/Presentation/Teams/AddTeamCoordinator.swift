//
//  AddTeamCoordinator.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/22.
//

import Foundation
import UIKit

class AddTeamCoordinator: Coordinator {
    
    weak var parentCoordinator: TeamsCoordinator?
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = AddTeamViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func start(isAdd: Bool = true, team: Teams) {
        let vc = AddTeamViewController.instantiate()
        vc.coordinator = self
        vc.isAdd = isAdd
        vc.team = team
        navigationController.pushViewController(vc, animated: true)
    }

    func didFinish(){
        parentCoordinator?.childDidFinish(self)
    }
}
