//
//  AddTeamViewController.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/22.
//

import UIKit
import CoreData

class AddTeamViewController: UIViewController, Storyboarded  {
    
    @IBOutlet weak var teamImageView: UIView!
    @IBOutlet weak var teamNameText: UITextField!
    @IBOutlet weak var photoImage: UIImageView!
    
    var coordinator: AddTeamCoordinator?
//    weak var coordinator: TeamsCoordinator?

    var isAdd = true
    var team: Teams? //要編輯的球隊
    var photoName: String?
    
    private var appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        teamNameText.delegate = self
        initUI()
        setupGesture()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        coordinator?.didFinish()
    }
}

extension AddTeamViewController{
    private func initUI(){
        self.title = isAdd ? "新增球隊" : "編輯球隊"
        if isAdd{
            self.title = "新增球隊"
            teamNameText.text = ""
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "儲存", style: .plain, target: self, action: #selector(saveTeam))
        }else{
            self.title = "編輯球隊"
            teamNameText.text = team?.name
            if let iconName = team?.iconName{
                photoImage.image = UIImage(named: iconName)
                setupPhotoImageConstraint()
            }
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "更新", style: .plain, target: self, action: #selector(updateTeam))
        }
    }
    
    @objc public func saveTeam() {
        if let text = teamNameText.text{
            if text.isEmpty{
                showCustomAlert(mode: .One, message: "請輸入隊名")
            }else{
                if isTeamNameRepeat(teamName: text){
                    showCustomAlert(mode: .One, message: "隊伍名稱重複")
                }else{
                    addTeam(name: text)
                }
            }
        }
    }
    
    @objc public func updateTeam() {
        if let text = teamNameText.text{
            if text.isEmpty{
                showCustomAlert(mode: .One, message: "請輸入隊名")
            }else{
                replaceTeam(newName: text)
            }
        }
    }

    private func setupGesture(){
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        teamImageView.addGestureRecognizer(gesture)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        showPhotoSelectView { [self] photoName in
            photoImage.image = UIImage(named: photoName)
            self.photoName = photoName
            setupPhotoImageConstraint()
        }
    }
    
    private func setupPhotoImageConstraint(){
        //設定圖片自動佈局約束條件
        let leadingConstraint = NSLayoutConstraint(item: photoImage!, attribute: .leading, relatedBy: .equal, toItem: photoImage.superview, attribute: .leading, multiplier: 1, constant: 0)
        leadingConstraint.isActive = true

        let trailingConstraint = NSLayoutConstraint(item: photoImage!, attribute: .trailing, relatedBy: .equal, toItem: photoImage.superview, attribute: .trailing, multiplier: 1, constant: 0)
        trailingConstraint.isActive = true

        let topConstraint = NSLayoutConstraint(item: photoImage!, attribute: .top, relatedBy: .equal, toItem: photoImage.superview, attribute: .top, multiplier: 1, constant: 0)
        topConstraint.isActive = true

        let bottomConstraint = NSLayoutConstraint(item: photoImage!, attribute: .bottom, relatedBy: .equal, toItem: photoImage.superview, attribute: .bottom, multiplier: 1, constant: 0)
        bottomConstraint.isActive = true
    }
    
    private func isTeamNameRepeat(teamName: String) -> Bool {
        let request = NSFetchRequest<Teams>(entityName: "Teams" )
        do {
            let results = try context.fetch(request)
            let repeatTeams = results.filter({$0.name == teamName})
            return repeatTeams.isEmpty ? false : true
        }catch{
            showCustomAlert(mode: .One, message: "\(error)")
            return true
        }
    }
    
    private func addTeam(name: String) {
        let team = NSEntityDescription.insertNewObject(forEntityName: "Teams", into: context ) as! Teams
        team.id = UUID().uuidString
        team.name = name
        if let photoName = photoName{
            team.iconName = photoName
        }
        saveContext()
    }
        
    private func saveContext () {
        if context.hasChanges {
            do {
                try context.save()
                showCustomNoBtnAlert(message: "新增成功") {
                    self.navigationController?.popViewController(animated: true)
                }
            } catch {
                showCustomNoBtnAlert(message: "新增失敗") {
                    self.navigationController?.popViewController(animated: true)
                }
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func replaceTeam(newName: String) {
        let request_team = NSFetchRequest<Teams>(entityName: "Teams" )
        // 將原本的team找出來（要編輯的team）
        request_team.predicate = NSPredicate(format: "id CONTAINS[cd] %@", team?.id ?? "")
        
        do {
            //更改隊名
            let data = try context.fetch(request_team)
            if data.count > 0{
                let team = data[0]
                team.name = newName
                if let photoName = photoName{
                    team.iconName = photoName
                }
            }
            
            updateContext()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    private func updateContext () {
        if context.hasChanges {
            do {
                try context.save()
                showCustomNoBtnAlert(message: "更新成功") {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            } catch {
                showCustomNoBtnAlert(message: "更新失敗") {
                    self.navigationController?.popToRootViewController(animated: true)
                }
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

//MARK: 輸入限制
extension AddTeamViewController: UITextFieldDelegate{
    //點擊空白處收回鍵盤
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == teamNameText{
            //限制總長度16位元組，8個中文或16個英數字
            let text = textField.text ?? ""
            var chineseDigis = 0
            var EnglishDigis = 0
            
            for i in 0..<text.count{
                let c: unichar = (text as NSString).character(at: i)
                if (c >= 0x4E00) {
                    chineseDigis += 1
                }else {
                    EnglishDigis += 1
                }
            }
            
            if chineseDigis >= 8 || EnglishDigis >= 16{
                return false
            }
            
            if ((chineseDigis * 2) + EnglishDigis) >= 16{
                return false
            }
            return true
        }
        
        return true
    }
}
