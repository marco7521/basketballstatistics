//
//  CustomNoBtnAlertViewController.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/22.
//

import UIKit

class CustomNoBtnAlertViewController: UIViewController {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var msgLabel: UILabel!
    
    var iconName = ""
    var message = ""
    var cancelAcction: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        setupGesture()
        
        Utility().delay(3, closure: {
            //出現燈箱後，若未點擊背景，三秒後消失，並執行cancelAcction
            self.presentingViewController?.dismiss(animated: true, completion: {
                self.cancelAcction?()
            })
        })
    }
    
    private func initUI(){
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        msgLabel.text = message
        iconImage.image = UIImage(named: iconName)
    }

    private func setupGesture(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(goBack))
        self.view.addGestureRecognizer(tap) //增加點擊手勢
    }
    
    @objc func goBack(){
        self.dismiss(animated: true) {
            self.cancelAcction?()
        }
    }
}
