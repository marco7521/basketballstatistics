//
//  ScoringAlertViewController.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/5/5.
//

import UIKit

class ScoringAlertViewController: UIViewController {

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var playerDetail: UILabel!
    @IBOutlet weak var ftm: UIButton!
    @IBOutlet weak var fta: UIButton!
    @IBOutlet weak var fgm: UIButton!
    @IBOutlet weak var fga: UIButton!
    @IBOutlet weak var tpm: UIButton!
    @IBOutlet weak var tpa: UIButton!
    @IBOutlet weak var pf: UIButton!
    @IBOutlet weak var tov: UIButton!
    @IBOutlet weak var oreb: UIButton!
    @IBOutlet weak var dreb: UIButton!
    @IBOutlet weak var ast: UIButton!
    @IBOutlet weak var stl: UIButton!
    @IBOutlet weak var blk: UIButton!
    
    var player: Players?
    var eventAction: ((Int) -> Void)?
    
    private var appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        setupBtnUI()
    }

    @IBAction func closedAction(_ sender: Any) {
        self.dismiss(animated: false)
    }
    
    @IBAction func scoringAction(_ sender: UIButton) {
        self.dismiss(animated: false){
            self.eventAction?(sender.tag)
        }
    }
}

extension ScoringAlertViewController{
    private func initUI(){
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        backgroundView.clipsToBounds = true
        backgroundView.layer.cornerRadius = 12
        backgroundView.layer.cornerCurve = .continuous
        
        if let player = player{
            playerDetail.text = "(\(player.number ?? "")) \(player.name ?? "")"
        }else{
            playerDetail.text = "全隊"
        }
    }
    
    private func setupBtnUI(){
        ftm.layer.cornerRadius = 8
        ftm.layer.cornerCurve = .continuous
        fta.layer.cornerRadius = 8
        fta.layer.cornerCurve = .continuous
        fgm.layer.cornerRadius = 8
        fgm.layer.cornerCurve = .continuous
        fga.layer.cornerRadius = 8
        fga.layer.cornerCurve = .continuous
        tpm.layer.cornerRadius = 8
        tpm.layer.cornerCurve = .continuous
        tpa.layer.cornerRadius = 8
        tpa.layer.cornerCurve = .continuous
        pf.layer.cornerRadius = 8
        pf.layer.cornerCurve = .continuous
        tov.layer.cornerRadius = 8
        tov.layer.cornerCurve = .continuous
        oreb.layer.cornerRadius = 8
        oreb.layer.cornerCurve = .continuous
        dreb.layer.cornerRadius = 8
        dreb.layer.cornerCurve = .continuous
        ast.layer.cornerRadius = 8
        ast.layer.cornerCurve = .continuous
        stl.layer.cornerRadius = 8
        stl.layer.cornerCurve = .continuous
        blk.layer.cornerRadius = 8
        blk.layer.cornerCurve = .continuous
        fta.layer.borderWidth = 1
        fta.layer.borderColor = UIColor.systemGreen.cgColor
        fga.layer.borderWidth = 1
        fga.layer.borderColor = UIColor.systemGreen.cgColor
        tpa.layer.borderWidth = 1
        tpa.layer.borderColor = UIColor.systemGreen.cgColor
    }
}
