//
//  TeamsSelectViewController.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/5/1.
//

import UIKit
import CoreData

class TeamsSelectViewController: UIViewController {

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var emptyView: UIView!
    
    var teams: [Teams]?
    var players: [Players]?
    var teamsSelectAcction: ((Teams, Int) -> Void)?

    private var appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        initUI()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchTeams()
        fetchPlayers()
    }
    
    @IBAction func closedAction(_ sender: Any) {
        dismiss(animated: true)
    }
    
    private func initUI(){
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        backgroundView.clipsToBounds = true
    }
    
    private func setupTableView(){
        tableView.backgroundView = emptyView
        if let teams = teams{
            if !teams.isEmpty{
                tableView.backgroundView = nil
            }
        }
    }
    
    private func fetchTeams() {
        let request = NSFetchRequest<Teams>(entityName: "Teams" )
        do {
            let results = try context.fetch(request)
            teams = results
            tableView.reloadData()
        }catch{
            showCustomAlert(mode: .One, message: "\(error)")
        }
        setupTableView()
    }
    
    private func fetchPlayers() {
        let request = NSFetchRequest<Players>(entityName: "Players" )
        do {
            let results = try context.fetch(request)
            players = results
            tableView.reloadData()
        }catch{
            showCustomAlert(mode: .One, message: "\(error)")
        }
    }
}

extension TeamsSelectViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamsTableViewCell", for: indexPath) as! TeamsTableViewCell
        
        cell.teamIcon.image = UIImage(named: "unknown") //未設定圖片就使用預設圖片
        
        let teamName = teams?[indexPath.row].name
        let iconName = teams?[indexPath.row].iconName ?? ""
        cell.teamNameLabel.text = teamName
        if !iconName.isEmpty{
            cell.teamIcon.image = UIImage(named: iconName)
        }
        
        let players = players?.filter({$0.teamId == teams?[indexPath.row].id})
        cell.playersCountLabel.text = "\(players?.count ?? 0)名隊員"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let players = players?.filter({$0.teamId == teams?[indexPath.row].id})

        if let team = teams?[indexPath.row] {
            self.dismiss(animated: true) {
                self.teamsSelectAcction?(team, players?.count ?? 0)
            }
        }
    }
}
