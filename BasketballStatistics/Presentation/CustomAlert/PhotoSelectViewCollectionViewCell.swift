//
//  PhotoSelectViewCollectionViewCell.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/27.
//

import UIKit

class PhotoSelectViewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iconImage: UIImageView!
}
