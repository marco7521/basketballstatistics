//
//  AddPlayerViewController.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/26.
//

import UIKit
import CoreData

class AddPlayerViewController: UIViewController {

    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var numberText: UITextField!
    
    @IBOutlet weak var noBtn: UIButton!
    @IBOutlet weak var yesBtn: UIButton!
    
    var isAdd = true
    var teamId: String?
    var players: [Players]?
    var editPlayer: Players?

    var yesButtonAction: (() -> Void)?

    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        nameText.delegate = self
        numberText.delegate = self
        initUI()
    }
    
    private func initUI(){
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        yesBtn.layer.cornerRadius = 8
        noBtn.layer.cornerRadius = 8
        if #available(iOS 13.0, *) {
            yesBtn.layer.cornerCurve = .continuous
            noBtn.layer.cornerCurve = .continuous
        }
        
        if !isAdd{
            nameText.text = editPlayer?.name
            numberText.text = editPlayer?.number
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if nameText.text!.isEmpty || numberText.text!.isEmpty {
            let controller = UIViewController.getLastPresentedViewController()
            controller?.showCustomAlert(mode: .One, message: "請輸入名字或背號")
        }else{
            if isAdd{
                if let players = players?.filter({$0.number == numberText.text!}){
                    if players.isEmpty{
                        addPlayer(name: nameText.text!, number: numberText.text!)
                    }else{
                        let controller = UIViewController.getLastPresentedViewController()
                        controller?.showCustomAlert(mode: .One, message: "輸入的背號重複！")
                    }
                }
            }else{
                if let players = players?.filter({$0.number == numberText.text!}){
                    //檢查背號沒有重複
                    if players.isEmpty{
                        updatePlayer(newName: nameText.text!, newNumber: numberText.text!)
                    }else{
                        let players2 = players.filter({$0.id == editPlayer?.id ?? ""})
                        //背號重複，檢查是不是當下編輯的球員
                        if players2.isEmpty{
                            let controller = UIViewController.getLastPresentedViewController()
                            controller?.showCustomAlert(mode: .One, message: "輸入的背號重複！")
                        }else{
                            updatePlayer(newName: nameText.text!, newNumber: numberText.text!)
                        }
                    }
                }
            }
        }
    }
    
    private func addPlayer(name: String, number: String) {
        let player = NSEntityDescription.insertNewObject(forEntityName: "Players", into: context ) as! Players
        player.teamId = teamId
        player.id = UUID().uuidString
        player.name = name
        player.number = number
        saveContext()
    }
        
    private func saveContext () {
        if context.hasChanges {
            do {
                try context.save()
                self.dismiss(animated: true) { [self] in
                    yesButtonAction?()
                }
            } catch {
                self.dismiss(animated: true) { [self] in
                    showCustomNoBtnAlert(message: "新增失敗")
                }
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    private func updatePlayer(newName: String, newNumber: String) {
        let request = NSFetchRequest<Players>(entityName: "Players" )
        // 將原本的team裡的player 找出來
        request.predicate = NSPredicate(format: "number = %@ AND teamId = %@", argumentArray: [editPlayer?.number ?? "", editPlayer?.teamId ?? ""])
        
        do {
            //更改球員歸屬的球隊
            let datas = try context.fetch(request)
            for data in datas{
                data.name = newName
                data.number = newNumber
            }
            
            updateContext()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }

    private func updateContext () {
        if context.hasChanges {
            do {
                try context.save()
                self.dismiss(animated: true) { [self] in
                    yesButtonAction?()
                }
            } catch {
                self.dismiss(animated: true) { [self] in
                    showCustomNoBtnAlert(message: "更新失敗")
                }
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

//MARK: 輸入限制
extension AddPlayerViewController: UITextFieldDelegate{
    //點擊空白處收回鍵盤
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == nameText && !string.isEmpty{
            //限制總長度20位元組，10個中文或20個英數字
            let text = textField.text ?? ""
            var chineseDigis = 0
            var EnglishDigis = 0
            
            for i in 0..<text.count{
                let c: unichar = (text as NSString).character(at: i)
                if (c >= 0x4E00) {
                    chineseDigis += 1
                }else {
                    EnglishDigis += 1
                }
            }
            
            if chineseDigis >= 10 || EnglishDigis >= 20{
                return false
            }
            
            if ((chineseDigis * 2) + EnglishDigis) >= 20{
                return false
            }
            return true
        }

        if textField == numberText{
            //輸入限制最多輸入2位數
            let text = textField.text!
            let len = text.count + string.count - range.length
            return len <= 2
        }

        return true
    }
}
