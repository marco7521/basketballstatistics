//
//  CustomAlertViewController.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/22.
//

import UIKit

class CustomAlertViewController: UIViewController {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var noBtn: UIButton!
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var yesOrNoStackView: UIStackView!
    @IBOutlet weak var confirmBtn: UIButton!

    var mode = Bool() //判斷按鈕有幾個
    var iconName = ""
    var isIcon = false
    var message = ""
    var yesButtonTitle = ""
    var noButtonTitle = ""
    var confirmButtonTitle = ""

    var yesButtonAction: (() -> Void)?
    var noButtonAction: (() -> Void)?
    var confirmButtonAction: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
    private func initUI(){
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        yesBtn.layer.cornerRadius = 8
        noBtn.layer.cornerRadius = 8
        confirmBtn.layer.cornerRadius = 8
        if #available(iOS 13.0, *) {
            yesBtn.layer.cornerCurve = .continuous
            noBtn.layer.cornerCurve = .continuous
            confirmBtn.layer.cornerCurve = .continuous
        }

        messageLabel.text = message
        yesBtn.setTitle(yesButtonTitle, for: .normal)
        noBtn.setTitle(noButtonTitle, for: .normal)
        confirmBtn.setTitle(confirmButtonTitle, for: .normal)

        if iconName.isEmpty {
            self.iconImage.isHidden = true
        }else{
            self.iconImage.isHidden = false
            iconImage.image = UIImage(named: iconName)
        }

        if mode{
            yesOrNoStackView.isHidden = mode
            confirmBtn.isHidden = !mode
        }else{
            yesOrNoStackView.isHidden = mode
            confirmBtn.isHidden = !mode
        }
    }
    @IBAction func yesAction(_ sender: Any) {
        self.dismiss(animated: true) { [self] in
            yesButtonAction?()
        }
    }
    
    @IBAction func noAction(_ sender: Any) {
        self.dismiss(animated: true) { [self] in
            noButtonAction?()
        }
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        self.dismiss(animated: true) { [self] in
            confirmButtonAction?()
        }
    }
}
