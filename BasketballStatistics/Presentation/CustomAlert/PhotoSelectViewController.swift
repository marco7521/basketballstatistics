//
//  PhotoSelectViewController.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/27.
//

import UIKit

class PhotoSelectViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    
    let photos = ["lion", "tiger", "dog", "cat", "bull", "panda", "wolf", "monkey", "bear", "fox", "koala", "toucan", "whale", "duck", "horse", "octopus", "snake", "turtle", "elephant", "sheep", "pig"]
    
    var photosSelectAcction: ((String) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        setupCollectionView()
    }
    
    private func initUI(){

    }
}

extension PhotoSelectViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    private func setupCollectionView(){
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 25, left: 20, bottom: 25, right: 20) //設定section的邊距
        collectionViewFlowLayout.minimumLineSpacing = 15 //設定cell與cell間的最小縱距
        collectionViewFlowLayout.minimumInteritemSpacing = 15 //設定cell與cell間的最小橫距
        collectionViewFlowLayout.scrollDirection = .vertical //設定縱向滑動
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoSelectViewCollectionViewCell", for: indexPath) as! PhotoSelectViewCollectionViewCell
        
        let photoName = photos[indexPath.row]
        cell.iconImage.image = UIImage(named: photoName)
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.collectionView.frame.width - 70  //減去section的左右邊距、cell與cell間的最小橫距(40+30)
        return CGSize(width: Int(width / 3), height: Int(width / 3)) //要給整數，否則多出的小數點會跑版
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
            self.photosSelectAcction?(self.photos[indexPath.row])
        }
    }
}
