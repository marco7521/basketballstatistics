//
//  GamesCoordinator.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/17.
//

import Foundation
import UIKit

class GamesCoordinator: Coordinator {
    
    weak var parentCoordinator: MainCoordinator?
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = GamesViewController.instantiate()
        vc.coordinator = self
        vc.tabBarItem = UITabBarItem(title: "比賽", image: UIImage(named: "Games"), tag: 0)
        navigationController.pushViewController(vc, animated: true)
    }
    
    func childDidFinish(_ child: Coordinator?){
        for (index, coordinator) in
            childCoordinators.enumerated() {
            if coordinator === child{
                childCoordinators.remove(at: index)
                break
            }
        }
    }

    func toAddGame(){
        let child = AddGameCoordinator(navigationController: navigationController)
        child.parentCoordinator = self
        childCoordinators.append(child)
        child.start()
    }

}
