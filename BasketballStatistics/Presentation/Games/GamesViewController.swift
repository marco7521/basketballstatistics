//
//  GamesViewController.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/15.
//

import UIKit
import CoreData

class GamesViewController: UIViewController, Storyboarded {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var emptyView: UIView!
    
    var gamesList: [Games]?
    var teamsList: [Teams]?
    var playersList: [Players]?
    var awayTeam: Teams?
    var awayTeamPlayers: [Players]?
    var homeTeam: Teams?
    var homeTeamPlayers: [Players]?

    var coordinator: GamesCoordinator?
    private var appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.global().async {   //非main thread負責讀取資料
            self.fetchGames()
            DispatchQueue.main.async { [self] in  //main thread負責更新UI
                if gamesList!.isEmpty{
                    tableView.backgroundView = emptyView
                }
                tableView.reloadData()
            }
        }
    }
}

extension GamesViewController{
    private func initUI(){
        self.title = "比賽"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "新增比賽", style: .plain, target: self, action: #selector(addGame))
    }
    
    @objc public func addGame() {
        coordinator?.toAddGame()
    }
    
    //MARK: 資料讀取
    private func fetchGames() {
        let games = NSFetchRequest<Games>(entityName: "Games" )
        let teams = NSFetchRequest<Teams>(entityName: "Teams" )
        let players = NSFetchRequest<Players>(entityName: "Players" )

        do {
            let games = try context.fetch(games)
            self.gamesList = games
            
            let teams = try context.fetch(teams)
            self.teamsList = teams

            let players = try context.fetch(players)
            self.playersList = players
            
        }catch{
            showCustomAlert(mode: .One, message: "\(error)")
        }
    }
}

//MARK: Table View
extension GamesViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gamesList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GamesTableViewCell", for: indexPath) as! GamesTableViewCell
        let game = gamesList?[indexPath.row]
        cell.awayTeamNameLabel.text = game?.awayTeamName
        cell.awayTeamImage.image = UIImage(named: game?.awayTeamIconName ?? "unknown")
        cell.awayTeamScoreLabel.text = game?.awayScore
        cell.homeTeamNameLabel.text = game?.homeTeamName
        cell.homeTeamImage.image = UIImage(named: game?.homeTeamIconName ?? "unknown")
        cell.homeTeamScoreLabel.text = game?.homeScore
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "StatisticsViewController") as! StatisticsViewController
        
        let gamesList = gamesList?[indexPath.row]
        let awayTeamId = gamesList?.awayTeamId
        let homeTeamId = gamesList?.homeTeamId

        for team in teamsList ?? []{
            if team.id == awayTeamId {
                awayTeam = team
            }
            if team.id == homeTeamId {
                homeTeam = team
            }
        }
        
        awayTeamPlayers = playersList?.filter({$0.teamId == gamesList?.awayTeamId})
        homeTeamPlayers = playersList?.filter({$0.teamId == gamesList?.homeTeamId})
        
        controller.gameId = gamesList?.id
        controller.awayTeam = awayTeam
        controller.homeTeam = homeTeam
        controller.awayTeamPlayers = awayTeamPlayers
        controller.homeTeamPlayers = homeTeamPlayers
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //Update memory
            let data = gamesList?.remove(at: indexPath.row)
            //update database
            deleteGame(id: data?.id ?? "")
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    //MARK: 刪除
    private func deleteGame(id: String) {
        let request_game = NSFetchRequest<Games>(entityName: "Games" )
        // 透過 predicate 將game找出來
        request_game.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let games = try context.fetch(request_game)
            for game in games {
                // 刪除game
                context.delete(game)
            }

            // 刪除後記得儲存Context
            appDelegate.saveContext()
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}
