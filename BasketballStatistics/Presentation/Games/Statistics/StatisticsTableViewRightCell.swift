//
//  StatisticsTableViewCell.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/5/6.
//

import UIKit

class StatisticsTableViewRightCell: UITableViewCell {

    @IBOutlet weak var ptsLabel: UILabel!
    @IBOutlet weak var rebLabel: UILabel!
    @IBOutlet weak var astLabel: UILabel!
    @IBOutlet weak var stlLabel: UILabel!
    @IBOutlet weak var blkLabel: UILabel!
    @IBOutlet weak var fgmLabel: UILabel!
    @IBOutlet weak var fgaLabel: UILabel!
    @IBOutlet weak var fgPercentLabel: UILabel!
    @IBOutlet weak var tpmLabel: UILabel!
    @IBOutlet weak var tpaLabel: UILabel!
    @IBOutlet weak var tpPercentLabel: UILabel!
    @IBOutlet weak var ftmLabel: UILabel!
    @IBOutlet weak var ftaLabel: UILabel!
    @IBOutlet weak var ftPercentLabel: UILabel!
    @IBOutlet weak var orebLabel: UILabel!
    @IBOutlet weak var drebLabel: UILabel!
    @IBOutlet weak var tovLabel: UILabel!
    @IBOutlet weak var pfLabel: UILabel!
    @IBOutlet weak var effLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
