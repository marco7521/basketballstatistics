//
//  StatisticsViewController.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/5/6.
//

import UIKit
import CoreData

class StatisticsViewController: UIViewController {

    @IBOutlet weak var awayTeamNameLabel: UILabel!
    @IBOutlet weak var awayTeamImg: UIImageView!
    @IBOutlet weak var awayTeamScoreLabel: UILabel!
    
    @IBOutlet weak var homeTeamNameLabel: UILabel!
    @IBOutlet weak var homeTeamImg: UIImageView!
    @IBOutlet weak var homeTeamScoreLabel: UILabel!
    
    @IBOutlet weak var segment: UISegmentedControl!
    
    @IBOutlet weak var tableViewLeftHeaderView: UIView!
    @IBOutlet weak var tableViewLeft: UITableView!
    @IBOutlet weak var tableViewRightHeaderView: UIView!
    @IBOutlet weak var tableViewRight: UITableView!

    @IBOutlet weak var totalPts: UILabel!
    @IBOutlet weak var totalRebs: UILabel!
    @IBOutlet weak var totalAsts: UILabel!
    @IBOutlet weak var totalStls: UILabel!
    @IBOutlet weak var totalBlks: UILabel!
    @IBOutlet weak var totalFGM: UILabel!
    @IBOutlet weak var totalFGA: UILabel!
    @IBOutlet weak var totalFGPercent: UILabel!
    @IBOutlet weak var total3PM: UILabel!
    @IBOutlet weak var total3PA: UILabel!
    @IBOutlet weak var total3PPercent: UILabel!
    @IBOutlet weak var totalFTM: UILabel!
    @IBOutlet weak var totalFTA: UILabel!
    @IBOutlet weak var totalFTPercent: UILabel!
    @IBOutlet weak var totalOrebs: UILabel!
    @IBOutlet weak var totalDrebs: UILabel!
    @IBOutlet weak var totalTovs: UILabel!
    @IBOutlet weak var totalPfs: UILabel!
    
    var gameId: String?
    var awayTeam: Teams?
    var awayTeamPlayers: [Players]?
    var awayBoxScores: [BoxScores]?
    var homeTeam: Teams?
    var homeTeamPlayers: [Players]?
    var homeBoxScores: [BoxScores]?

    private var appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        setupTableView()
        setupSegment()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.global().async {   //非main thread負責讀取資料
            self.fetchBoxScores()
            DispatchQueue.main.async {  //main thread負責更新UI
                self.setupScoreLabel()
                self.setupTotalStatistics()
            }
        }
    }
    
    @IBAction func SegmentAction(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            tableViewLeftHeaderView.backgroundColor = UIColor(named: "IconRed")
            tableViewRightHeaderView.backgroundColor = UIColor(named: "IconRed")
        }else{
            tableViewLeftHeaderView.backgroundColor = UIColor(named: "IconBlue")
            tableViewRightHeaderView.backgroundColor = UIColor(named: "IconBlue")
        }
        
        setupTotalStatistics()
        tableViewLeft.reloadData()
        tableViewRight.reloadData()
    }
}

extension StatisticsViewController{
    private func initUI() {
        self.title = "統計數據"
        
        awayTeamNameLabel.text = awayTeam?.name
        awayTeamImg.image = UIImage(named: awayTeam?.iconName ?? "unknown")
        homeTeamNameLabel.text = homeTeam?.name
        homeTeamImg.image = UIImage(named: homeTeam?.iconName ?? "unknown")
    }
    
    private func setupSegment() {
        segment.setTitle(awayTeam?.name, forSegmentAt: 0)
        segment.setTitle(homeTeam?.name, forSegmentAt: 1)
    }
    
    //MARK: 資料讀取
    private func fetchBoxScores() {
        let awayBoxScores = NSFetchRequest<BoxScores>(entityName: "BoxScores" )
        awayBoxScores.predicate = NSPredicate(format: "gameId = %@ AND teamId = %@", argumentArray: [gameId ?? "", awayTeam?.id ?? ""])
        let homeBoxScores = NSFetchRequest<BoxScores>(entityName: "BoxScores" )
        homeBoxScores.predicate = NSPredicate(format: "gameId = %@ AND teamId = %@", argumentArray: [gameId ?? "", homeTeam?.id ?? ""])

        do {
            let awayBoxScores = try context.fetch(awayBoxScores)
            self.awayBoxScores = awayBoxScores
            let homeBoxScores = try context.fetch(homeBoxScores)
            self.homeBoxScores = homeBoxScores
        }catch{
            showCustomAlert(mode: .One, message: "\(error)")
        }
    }
    
    //MARK: 總分顯示
    private func setupScoreLabel(){
        var awayScore = 0
        var homeScore = 0
        
        for data in awayBoxScores! {
            awayScore = awayScore + Int(data.pts)
        }
        
        for data in homeBoxScores! {
            homeScore = homeScore + Int(data.pts)
        }
        awayTeamScoreLabel.text = String(awayScore)
        homeTeamScoreLabel.text = String(homeScore)
    }
    
    //MARK: 全隊數據
    private func setupTotalStatistics(){
        var pts = 0
        var reb = 0
        var ast = 0
        var stl = 0
        var blk = 0
        var fgm = 0
        var fga = 0
        var fgPercent = 0
        var tpm = 0
        var tpa = 0
        var tpPercent = 0
        var ftm = 0
        var fta = 0
        var ftPercent = 0
        var oreb = 0
        var dreb = 0
        var tov = 0
        var pf = 0

        if segment.selectedSegmentIndex == 0 {
            for data in awayBoxScores!{
                pts = pts + Int(data.pts)
                reb = reb + Int(data.reb)
                ast = ast + Int(data.ast)
                stl = stl + Int(data.stl)
                blk = blk + Int(data.blk)
                fgm = fgm + Int(data.fgm)
                fga = fga + Int(data.fga)
                tpm = tpm + Int(data.tpm)
                tpa = tpa + Int(data.tpa)
                ftm = ftm + Int(data.ftm)
                fta = fta + Int(data.fta)
                oreb = oreb + Int(data.oreb)
                dreb = dreb + Int(data.dreb)
                tov = tov + Int(data.tov)
                pf = pf + Int(data.pf)
            }
        }else{
            for data in homeBoxScores!{
                pts = pts + Int(data.pts)
                reb = reb + Int(data.reb)
                ast = ast + Int(data.ast)
                stl = stl + Int(data.stl)
                blk = blk + Int(data.blk)
                fgm = fgm + Int(data.fgm)
                fga = fga + Int(data.fga)
                tpm = tpm + Int(data.tpm)
                tpa = tpa + Int(data.tpa)
                ftm = ftm + Int(data.ftm)
                fta = fta + Int(data.fta)
                oreb = oreb + Int(data.oreb)
                dreb = dreb + Int(data.dreb)
                tov = tov + Int(data.tov)
                pf = pf + Int(data.pf)
            }
        }
        
        fgPercent = fga == 0 ? 0 : Int(Float(fgm) / Float(fga) * 100)
        tpPercent = tpa == 0 ? 0 : Int(Float(tpm) / Float(tpa) * 100)
        ftPercent = fta == 0 ? 0 : Int(Float(ftm) / Float(fta) * 100)

        totalPts.text = String(pts)
        totalRebs.text = String(reb)
        totalAsts.text = String(ast)
        totalStls.text = String(stl)
        totalBlks.text = String(blk)
        totalFGM.text = String(fgm)
        totalFGA.text = String(fga)
        totalFGPercent.text = "\(fgPercent)%"
        total3PM.text = String(tpm)
        total3PA.text = String(tpa)
        total3PPercent.text = "\(tpPercent)%"
        totalFTM.text = String(ftm)
        totalFTA.text = String(fta)
        totalFTPercent.text = "\(ftPercent)%"
        totalOrebs.text = String(oreb)
        totalDrebs.text = String(dreb)
        totalTovs.text = String(tov)
        totalPfs.text = String(pf)
    }
}

//MARK: Table View
extension StatisticsViewController: UITableViewDelegate, UITableViewDataSource{
    private func setupTableView() {
        tableViewLeft.delegate = self
        tableViewLeft.dataSource = self
        tableViewRight.delegate = self
        tableViewRight.dataSource = self
        tableViewLeft.rowHeight = 28
        tableViewRight.rowHeight = 28
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segment.selectedSegmentIndex == 0 {
            return awayTeamPlayers?.count ?? 0
        }else{
            return homeTeamPlayers?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewLeft{
            let cell = tableView.dequeueReusableCell(withIdentifier: "StatisticsTableViewLeftCell", for: indexPath) as! StatisticsTableViewLeftCell
            if segment.selectedSegmentIndex == 0 {
                let playerName = awayTeamPlayers?[indexPath.row].name ?? ""
                let playerNumber = awayTeamPlayers?[indexPath.row].number ?? ""
                cell.playerNameLabel.text = "[\(playerNumber)] \(playerName)"
            }else{
                let playerName = homeTeamPlayers?[indexPath.row].name ?? ""
                let playerNumber = homeTeamPlayers?[indexPath.row].number ?? ""
                cell.playerNameLabel.text = "[\(playerNumber)] \(playerName)"
            }
            
            return cell

        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "StatisticsTableViewRightCell", for: indexPath) as! StatisticsTableViewRightCell
            
            var pid = "" //球員id
            var pts = 0
            var reb = 0
            var ast = 0
            var stl = 0
            var blk = 0
            var fgm = 0
            var fga = 0
            var fgPercent = 0
            var tpm = 0
            var tpa = 0
            var tpPercent = 0
            var ftm = 0
            var fta = 0
            var ftPercent = 0
            var oreb = 0
            var dreb = 0
            var tov = 0
            var pf = 0

            if segment.selectedSegmentIndex == 0 {
                pid = awayTeamPlayers?[indexPath.row].id ?? ""
                let playerBoxScores = awayBoxScores?.filter({$0.playerId == pid})
                for data in playerBoxScores!{
                    pts = pts + Int(data.pts)
                    reb = reb + Int(data.reb)
                    ast = ast + Int(data.ast)
                    stl = stl + Int(data.stl)
                    blk = blk + Int(data.blk)
                    fgm = fgm + Int(data.fgm)
                    fga = fga + Int(data.fga)
                    tpm = tpm + Int(data.tpm)
                    tpa = tpa + Int(data.tpa)
                    ftm = ftm + Int(data.ftm)
                    fta = fta + Int(data.fta)
                    oreb = oreb + Int(data.oreb)
                    dreb = dreb + Int(data.dreb)
                    tov = tov + Int(data.tov)
                    pf = pf + Int(data.pf)
                }
                
            }else{
                pid = homeTeamPlayers?[indexPath.row].id ?? ""
                let playerBoxScores = homeBoxScores?.filter({$0.playerId == pid})
                for data in playerBoxScores!{
                    pts = pts + Int(data.pts)
                    reb = reb + Int(data.reb)
                    ast = ast + Int(data.ast)
                    stl = stl + Int(data.stl)
                    blk = blk + Int(data.blk)
                    fgm = fgm + Int(data.fgm)
                    fga = fga + Int(data.fga)
                    tpm = tpm + Int(data.tpm)
                    tpa = tpa + Int(data.tpa)
                    ftm = ftm + Int(data.ftm)
                    fta = fta + Int(data.fta)
                    oreb = oreb + Int(data.oreb)
                    dreb = dreb + Int(data.dreb)
                    tov = tov + Int(data.tov)
                    pf = pf + Int(data.pf)
                }
            }

            fgPercent = fga == 0 ? 0 : Int(Float(fgm) / Float(fga) * 100)
            tpPercent = tpa == 0 ? 0 : Int(Float(tpm) / Float(tpa) * 100)
            ftPercent = fta == 0 ? 0 : Int(Float(ftm) / Float(fta) * 100)

            cell.ptsLabel.text = String(pts)
            cell.rebLabel.text = String(reb)
            cell.astLabel.text = String(ast)
            cell.stlLabel.text = String(stl)
            cell.blkLabel.text = String(blk)
            cell.fgmLabel.text = String(fgm)
            cell.fgaLabel.text = String(fga)
            cell.fgPercentLabel.text = "\(fgPercent)%"
            cell.tpmLabel.text = String(tpm)
            cell.tpaLabel.text = String(tpa)
            cell.tpPercentLabel.text = "\(tpPercent)%"
            cell.ftmLabel.text = String(ftm)
            cell.ftaLabel.text = String(fta)
            cell.ftPercentLabel.text = "\(ftPercent)%"
            cell.orebLabel.text = String(oreb)
            cell.drebLabel.text = String(dreb)
            cell.tovLabel.text = String(tov)
            cell.pfLabel.text = String(pf)

            return cell
        }
    }
}

//MARK: Scroll View
extension StatisticsViewController: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tableViewLeft == scrollView {
            tableViewRight.contentOffset = tableViewLeft.contentOffset
        }else if tableViewRight == scrollView {
            tableViewLeft.contentOffset = tableViewRight.contentOffset
        }
    }
}
