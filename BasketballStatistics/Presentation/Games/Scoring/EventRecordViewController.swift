//
//  EventRecordViewController.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/5/5.
//

import UIKit
import CoreData

class EventRecordViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var gameId: String?
    var boxScores: [BoxScores]?
    var awayTeam: Teams?
    var homeTeam: Teams?
    var awayPlayers: [Players]?
    var homePlayers: [Players]?

    private var appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        initUI()
        DispatchQueue.global().async {   //非main thread負責讀取資料
            self.fetchBoxScores()
            DispatchQueue.main.async {  //main thread負責更新UI
                self.scrollToBottom()
            }
        }
    }
    
    private func initUI(){
        self.title = "事件記錄"
    }
    
    private func fetchBoxScores() {
        let boxScores = NSFetchRequest<BoxScores>(entityName: "BoxScores" )
        boxScores.predicate = NSPredicate(format: "gameId = %@", gameId ?? "")
        
        let awayPlayers = NSFetchRequest<Players>(entityName: "Players" )
        awayPlayers.predicate = NSPredicate(format: "teamId = %@", awayTeam?.id ?? "")
        
        let homePlayers = NSFetchRequest<Players>(entityName: "Players" )
        homePlayers.predicate = NSPredicate(format: "teamId = %@", homeTeam?.id ?? "")

        do {
            let boxScores = try context.fetch(boxScores)
            self.boxScores = boxScores
            let awayPlayers = try context.fetch(awayPlayers)
            self.awayPlayers = awayPlayers
            let homePlayers = try context.fetch(homePlayers)
            self.homePlayers = homePlayers
        }catch{
            showCustomAlert(mode: .One, message: "\(error)")
        }
    }
}

extension EventRecordViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return boxScores?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventRecordTableViewCell", for: indexPath) as! EventRecordTableViewCell
        let boxScores = boxScores?[indexPath.row]
        var team: Teams?
        var player: Players?
        
        let awayplayer = awayPlayers?.filter({$0.id == boxScores?.playerId})
        if !awayplayer!.isEmpty {
            player = awayplayer?[0]
        }
        let homeplayer = homePlayers?.filter({$0.id == boxScores?.playerId})
        if !homeplayer!.isEmpty {
            player = homeplayer?[0]
        }
        
        if boxScores?.teamId == awayTeam?.id {
            team = awayTeam
            cell.teamNameLabel?.textColor = UIColor(named: "IconRed")
            cell.numberLabel?.textColor = UIColor(named: "IconRed")
            cell.nameLabel?.textColor = UIColor(named: "IconRed")
            cell.eventLabel.backgroundColor = UIColor(named: "IconRed")
        }else{
            team = homeTeam
            cell.teamNameLabel?.textColor = UIColor(named: "IconBlue")
            cell.numberLabel?.textColor = UIColor(named: "IconBlue")
            cell.nameLabel?.textColor = UIColor(named: "IconBlue")
            cell.eventLabel.backgroundColor = UIColor(named: "IconBlue")
        }
        
        cell.teamNameLabel.text = team?.name
        cell.numberLabel.text = "[\(player?.number ?? "全隊")]"
        cell.nameLabel.text = player?.name

        if boxScores?.fta == 1{
            if boxScores?.fta == boxScores?.ftm{
                cell.eventLabel.text = "罰球一分"
            }else{
                cell.eventLabel.text = "罰球未進"
            }
        }
//        0 1 0 0
//        1 1 0 0
//        1 1 1 1
//        0 1 0 1
        if boxScores?.fga == 1{
            if boxScores?.fga == boxScores?.fgm{
                if boxScores?.tpa == 1 {
                    cell.eventLabel.text = "三分"
                }else{
                    cell.eventLabel.text = "二分"
                }
            }else{
                if boxScores?.tpa == 1 {
                    cell.eventLabel.text = "三分未進"
                }else{
                    cell.eventLabel.text = "投籃未進"
                }
            }
        }
        
        if boxScores?.pf == 1{
            cell.eventLabel.text = "犯規"
        }
        
        if boxScores?.tov == 1{
            cell.eventLabel.text = "失誤"
        }
        
        if boxScores?.dreb == 1{
            cell.eventLabel.text = "防守籃板"
        }

        if boxScores?.oreb == 1{
            cell.eventLabel.text = "進攻籃板"
        }

        if boxScores?.ast == 1{
            cell.eventLabel.text = "助攻"
        }

        if boxScores?.stl == 1{
            cell.eventLabel.text = "抄截"
        }
        
        if boxScores?.blk == 1{
            cell.eventLabel.text = "阻攻"
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //Update memory
            let data = boxScores?.remove(at: indexPath.row)
            //update database
            deleteData(id: data?.id ?? "")
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    //MARK: 刪除資料
    private func deleteData(id: String) {
        let request = NSFetchRequest<BoxScores>(entityName: "BoxScores" )
        // 透過 predicate 將team找出來
        request.predicate = NSPredicate(format: "id = %@", id)

        do {
            let boxScores = try context.fetch(request)
            for data in boxScores {
                context.delete(data)
            }
            // 刪除後記得儲存Context
            appDelegate.saveContext()

        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    ///移動到最底部
    func scrollToBottom(){
        if !boxScores!.isEmpty{
            let indexPath = IndexPath(row: self.boxScores!.count - 1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
    }
}
