//
//  EventRecordTableViewCell.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/5/6.
//

import UIKit

class EventRecordTableViewCell: UITableViewCell {

    @IBOutlet weak var teamNameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var eventLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        eventLabel.layer.masksToBounds = true
        eventLabel.layer.cornerRadius = 3
        eventLabel.layer.cornerCurve = .continuous
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
