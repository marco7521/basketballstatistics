//
//  ScoringCoordinator.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/5/1.
//

import Foundation
import UIKit

class ScoringCoordinator: Coordinator {
    
    weak var parentCoordinator: AddGameCoordinator?
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = ScoringViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func start(away: Teams, home: Teams) {
        let vc = ScoringViewController.instantiate()
        vc.coordinator = self
        vc.awayTeam = away
        vc.homeTeam = home
        navigationController.pushViewController(vc, animated: true)
    }
    
    func didFinish(){
        parentCoordinator?.childDidFinish(self)
    }
}
