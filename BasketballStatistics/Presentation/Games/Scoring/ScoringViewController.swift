//
//  ScoringViewController.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/5/1.
//

import UIKit
import CoreData

class ScoringViewController: UIViewController, Storyboarded  {
    
    @IBOutlet weak var awayTeamNameLabel: UILabel!
    @IBOutlet weak var awayTeamImg: UIImageView!
    @IBOutlet weak var awayScoreLabel: UILabel!
    
    @IBOutlet weak var homeTeamNameLabel: UILabel!
    @IBOutlet weak var homeTeamImg: UIImageView!
    @IBOutlet weak var homeScoreLabel: UILabel!
    
    @IBOutlet weak var awayTableView: UITableView!
    @IBOutlet weak var homeTableView: UITableView!
    
    var coordinator: ScoringCoordinator?
    private var appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    var awayBoxScores: [BoxScores]?
    var homeBoxScores: [BoxScores]?
    var awayTeam: Teams?
    var awayTeamPlayers: [Players]?
    var homeTeam: Teams?
    var homeTeamPlayers: [Players]?
    let gameId = UUID().uuidString
    var awayScore = 0
    var homeScore = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        awayTableView.delegate = self
        awayTableView.dataSource = self
        homeTableView.delegate = self
        homeTableView.dataSource = self
        initUI()
        fetchPlayers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        coordinator?.didFinish()
    }
    
    @IBAction func toStatistics(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "StatisticsViewController") as! StatisticsViewController
        controller.gameId = gameId
        controller.awayTeam = awayTeam
        controller.homeTeam = homeTeam
        controller.awayTeamPlayers = awayTeamPlayers
        controller.homeTeamPlayers = homeTeamPlayers
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func endGameAction(_ sender: Any) {
        showCustomAlert(mode: .Two, message: "是否結束比賽並儲存統計數據？", yesBtnAction: {
            self.saveGameStatistics()
            self.navigationController?.popToRootViewController(animated: false)
        })
    }
}

extension ScoringViewController {
    private func initUI(){
        self.title = "數據統計"
        self.navigationItem.setHidesBackButton(true, animated: false) //隱藏返回鍵
        awayTeamNameLabel.text = awayTeam?.name
        awayTeamImg.image = UIImage(named: awayTeam?.iconName ?? "unknown")
        awayScoreLabel.text = "0"
        
        homeTeamNameLabel.text = homeTeam?.name
        homeTeamImg.image = UIImage(named: homeTeam?.iconName ?? "unknown")
        homeScoreLabel.text = "0"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "事件記錄", style: .plain, target: self, action: #selector(eventRecord))
    }
    
    @objc public func eventRecord() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "EventRecordViewController") as! EventRecordViewController
        controller.gameId = gameId
        controller.awayTeam = awayTeam
        controller.homeTeam = homeTeam
        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func fetchPlayers() {
        let request = NSFetchRequest<Players>(entityName: "Players" )
        do {
            let results = try context.fetch(request)
            let awayPlayers = results.filter({$0.teamId == awayTeam?.id})
            let homePlayers = results.filter({$0.teamId == homeTeam?.id})
            awayTeamPlayers = awayPlayers
            homeTeamPlayers = homePlayers
            awayTableView.reloadData()
            homeTableView.reloadData()
        }catch{
            showCustomAlert(mode: .One, message: "\(error)")
        }
    }

    //MARK: 儲存比賽
    private func saveGameStatistics() {
        let game = NSEntityDescription.insertNewObject(forEntityName: "Games", into: context ) as! Games
        game.id = gameId
        game.awayScore = String(awayScore)
        game.awayTeamId = awayTeam?.id
        game.awayTeamName = awayTeam?.name
        game.awayTeamIconName = awayTeam?.iconName
        game.homeScore = String(homeScore)
        game.homeTeamId = homeTeam?.id
        game.homeTeamName = homeTeam?.name
        game.homeTeamIconName = homeTeam?.iconName
        saveContext()
    }

    //MARK: 數據記錄儲存
    private func eventRecording(tag: Int, player: Players? = nil, team: Teams? = nil) {
        let boxScores = NSEntityDescription.insertNewObject(forEntityName: "BoxScores", into: context ) as! BoxScores
        boxScores.id = UUID().uuidString
        boxScores.gameId = gameId
        boxScores.teamId = team == nil ? player?.teamId : team?.id
//        boxScores.teamId = player?.teamId
        boxScores.playerId = player?.id

        switch tag{
        case 0: //罰球進
            boxScores.fta = 1
            boxScores.ftm = 1
            boxScores.pts = 1
            
        case 1: //罰球未進
            boxScores.fta = 1

        case 2: //二分進
            boxScores.fga = 1
            boxScores.fgm = 1
            boxScores.pts = 2

        case 3: //二分未進
            boxScores.fga = 1
            
        case 4: //三分進
            boxScores.fga = 1
            boxScores.fgm = 1
            boxScores.tpa = 1
            boxScores.tpm = 1
            boxScores.pts = 3

        case 5: //三分未進
            boxScores.fga = 1
            boxScores.tpa = 1

        case 6: //犯規
            boxScores.pf = 1
            
        case 7: //失誤
            boxScores.tov = 1
            
        case 8: //進攻籃板
            boxScores.oreb = 1
            boxScores.reb = 1

        case 9: //防守籃板
            boxScores.dreb = 1
            boxScores.reb = 1
            
        case 10: //助攻
            boxScores.ast = 1
            
        case 11: //抄截
            boxScores.stl = 1
            
        case 12: //阻攻
            boxScores.blk = 1
            
        default:
            break
        }
        
        saveContext()
    }
    
    private func saveContext () {
        if context.hasChanges {
            do {
                try context.save()
                updateData()
//                showCustomNoBtnAlert(message: "新增成功") {
//                    self.navigationController?.popViewController(animated: true)
//                }
            } catch {
                let nserror = error as NSError
                showCustomAlert(mode: .One, message: "Unresolved error \(nserror), \(nserror.userInfo)")
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    private func updateData(){
        let away = NSFetchRequest<BoxScores>(entityName: "BoxScores" )
        away.predicate = NSPredicate(format: "gameId = %@ AND teamId = %@", argumentArray: [gameId, awayTeam?.id ?? ""])
        let home = NSFetchRequest<BoxScores>(entityName: "BoxScores" )
        home.predicate = NSPredicate(format: "gameId = %@ AND teamId = %@", argumentArray: [gameId, homeTeam?.id ?? ""])
        
        do {
            awayScore = 0
            homeScore = 0
            let awayBoxScores = try context.fetch(away)
            let homeBoxScores = try context.fetch(home)
            self.awayBoxScores = awayBoxScores
            self.homeBoxScores = homeBoxScores
            for BoxScores in awayBoxScores{
                awayScore = awayScore + Int(BoxScores.pts)
            }
            for BoxScores in homeBoxScores{
                homeScore = homeScore + Int(BoxScores.pts)
            }
            awayScoreLabel.text = String(awayScore)
            homeScoreLabel.text = String(homeScore)
            
            awayTableView.reloadData()
            homeTableView.reloadData()
        }catch{
            showCustomAlert(mode: .One, message: "\(error)")
        }
    }
}


extension ScoringViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == awayTableView{
            if let awayTeamPlayers = awayTeamPlayers{
                if awayTeamPlayers.isEmpty{
                    return 1
                }else{
                    return awayTeamPlayers.count
                }
            }
        }
        
        if tableView == homeTableView{
            if let homeTeamPlayers = homeTeamPlayers{
                if homeTeamPlayers.isEmpty{
                    return 1
                }else{
                    return homeTeamPlayers.count
                }
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == awayTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScoringTableViewCell", for: indexPath) as! ScoringTableViewCell
            
            if (awayTeamPlayers ?? []).isEmpty{
                cell.nameLabel.text = "全隊"
                let playerBoxScores = awayBoxScores
                var score = 0
                var foul = 0
                for data in playerBoxScores!{
                    score = score + Int(data.pts)
                    foul = foul + Int(data.pf)
                }
                cell.scoreLabel.text = String(score)
                cell.foulLabel.text = String(foul)

                return cell

            }else{
                let player = awayTeamPlayers?[indexPath.row]
                cell.numberLabel.text = player?.number
                cell.nameLabel.text = player?.name
                let playerBoxScores = awayBoxScores?.filter({$0.playerId == player?.id})
                var score = 0
                var foul = 0
                for data in playerBoxScores!{
                    score = score + Int(data.pts)
                    foul = foul + Int(data.pf)
                }
                cell.scoreLabel.text = String(score)
                cell.foulLabel.text = String(foul)
                
                return cell
            }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScoringTableViewCell", for: indexPath) as! ScoringTableViewCell
            
            if (homeTeamPlayers ?? []).isEmpty{
                cell.nameLabel.text = "全隊"
                let playerBoxScores = homeBoxScores
                var score = 0
                var foul = 0
                for data in playerBoxScores!{
                    score = score + Int(data.pts)
                    foul = foul + Int(data.pf)
                }
                cell.scoreLabel.text = String(score)
                cell.foulLabel.text = String(foul)

                return cell

            }else{
                let player = homeTeamPlayers?[indexPath.row]
                cell.numberLabel.text = player?.number
                cell.nameLabel.text = player?.name
                let playerBoxScores = homeBoxScores?.filter({$0.playerId == player?.id})
                var score = 0
                var foul = 0
                for data in playerBoxScores!{
                    score = score + Int(data.pts)
                    foul = foul + Int(data.pf)
                }
                cell.scoreLabel.text = String(score)
                cell.foulLabel.text = String(foul)

                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == awayTableView{
            if let awayTeamPlayers = awayTeamPlayers {
                if awayTeamPlayers.isEmpty{
                    showScoringView { [self] tag in
                        self.eventRecording(tag: tag, team: awayTeam)
                    }
                }else{
                    let awayPlayer = awayTeamPlayers[indexPath.row]
                    showScoringView(player: awayPlayer){ [self] tag in
                        self.eventRecording(tag: tag, player: awayPlayer)
                    }
                }
            }
        }else{
            if let homeTeamPlayers = homeTeamPlayers {
                if homeTeamPlayers.isEmpty{
                    showScoringView { [self] tag in
                        self.eventRecording(tag: tag, team: homeTeam)
                    }
                }else{
                    let homePlayer = homeTeamPlayers[indexPath.row]
                    showScoringView(player: homePlayer) { [self] tag in
                        self.eventRecording(tag: tag, player: homePlayer)
                    }
                }
            }
        }
    }
}
