//
//  AddGameViewController.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/5/1.
//

import UIKit

class AddGameViewController: UIViewController, Storyboarded  {
    
    @IBOutlet weak var awayTeamImageView: UIView!
    @IBOutlet weak var awayTeamImg: UIImageView!
    @IBOutlet weak var awayTeamNameLabel: UILabel!
    @IBOutlet weak var homeTeamImageView: UIView!
    @IBOutlet weak var homeTeamImg: UIImageView!
    @IBOutlet weak var homeTeamNameLabel: UILabel!
    
    var awayTeam: Teams?
    var homeTeam: Teams?
    var awayPlayersCount = 0
    var homePlayersCount = 0

    var coordinator: AddGameCoordinator?

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        setupGesture()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        coordinator?.didFinish()
    }

    @IBAction func startGameAction(_ sender: Any) {
        if awayTeam == nil{
            showCustomAlert(mode: .One, message: "請選擇客隊球隊")
        }else if homeTeam == nil{
            showCustomAlert(mode: .One, message: "請選擇主隊球隊")
        }else if awayTeam == homeTeam{
            showCustomAlert(mode: .One, message: "不可選擇同一支球隊")
        }else{
            if awayPlayersCount == 0 &&  homePlayersCount == 0 {
                showCustomAlert(mode: .Two,
                                message: "\(awayTeam?.name ?? "")未創建球員，\(homeTeam?.name ?? "")未創建球員，將只統計全隊數據",
                                yesBtnTitle: "確定且繼續",
                                noBtnTitle: "取消",
                                yesBtnAction: { [self] in
                    if let awayTeam = awayTeam, let homeTeam = homeTeam{
                        self.coordinator?.toScoringView(away: awayTeam, home: homeTeam)
                    }
                })
            }else if awayPlayersCount == 0 {
                showCustomAlert(mode: .Two,
                                message: "\(awayTeam?.name ?? "")未創建球員，將只統計全隊數據",
                                yesBtnTitle: "確定且繼續",
                                noBtnTitle: "取消",
                                yesBtnAction: { [self] in
                    if let awayTeam = awayTeam, let homeTeam = homeTeam{
                        self.coordinator?.toScoringView(away: awayTeam, home: homeTeam)
                    }
                })
            }else if homePlayersCount == 0 {
                showCustomAlert(mode: .Two,
                                message: "\(homeTeam?.name ?? "")未創建球員，將只統計全隊數據",
                                yesBtnTitle: "確定且繼續",
                                noBtnTitle: "取消",
                                yesBtnAction: { [self] in
                    if let awayTeam = awayTeam, let homeTeam = homeTeam{
                        self.coordinator?.toScoringView(away: awayTeam, home: homeTeam)
                    }
                })
            }else{
                if let awayTeam = awayTeam, let homeTeam = homeTeam{
                    self.coordinator?.toScoringView(away: awayTeam, home: homeTeam)
                }
            }
        }
    }
}


extension AddGameViewController{
    private func initUI(){
        self.title = "新增比賽"
    }

    private func setupGesture(){
        let awayTap = UITapGestureRecognizer(target: self, action: #selector(handleAwayTap(_:)))
        awayTeamImageView.addGestureRecognizer(awayTap)
        
        let homeTap = UITapGestureRecognizer(target: self, action: #selector(handleHomeTap(_:)))
        homeTeamImageView.addGestureRecognizer(homeTap)
    }
    
    @objc func handleAwayTap(_ sender: UITapGestureRecognizer) {
        showTeamsSelectView { [self] team, count in
            awayTeam = team
            awayTeamImg.image = UIImage(named: team.iconName ?? "unknown")
            awayTeamNameLabel.text = team.name
            awayPlayersCount = count
        }
    }

    @objc func handleHomeTap(_ sender: UITapGestureRecognizer) {
        showTeamsSelectView { [self] team, count in
            homeTeam = team
            homeTeamImg.image = UIImage(named: team.iconName ?? "unknown")
            homeTeamNameLabel.text = team.name
            homePlayersCount = count
        }
    }
}
