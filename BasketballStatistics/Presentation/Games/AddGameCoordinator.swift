//
//  AddGameCoordinator.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/5/1.
//

import Foundation
import UIKit

class AddGameCoordinator: Coordinator {
    
    weak var parentCoordinator: GamesCoordinator?
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = AddGameViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
//    func start(isAdd: Bool = true, team: Teams) {
//        let vc = AddTeamViewController.instantiate()
//        navigationController.pushViewController(vc, animated: true)
//    }

    func didFinish(){
        parentCoordinator?.childDidFinish(self)
    }
    
    func childDidFinish(_ child: Coordinator?){
        for (index, coordinator) in
            childCoordinators.enumerated() {
            if coordinator === child{
                childCoordinators.remove(at: index)
                break
            }
        }
    }

    func toScoringView(away: Teams, home: Teams){
        let child = ScoringCoordinator(navigationController: navigationController)
        child.parentCoordinator = self
        childCoordinators.append(child)
        child.start(away: away, home: home)
    }
}
