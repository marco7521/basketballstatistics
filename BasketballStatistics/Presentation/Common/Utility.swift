//
//  Utility.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/22.
//

import Foundation
import UIKit

///一般共用
class Utility{
    ///延遲後執行
    /// - parameter delay:延遲時間
    /// - parameter closure:延遲後所要執行的動作
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
}
