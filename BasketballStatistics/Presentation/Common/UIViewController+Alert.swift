//
//  UIViewController+Alert.swift
//  BasketballStatistics
//
//  Created by 鄭勝文 on 2022/4/22.
//

import Foundation
import UIKit

enum ButtonMode {
    ///一個按鈕
    case One
    ///兩個按鈕
    case Two
}

extension UIViewController {
    
    ///客製化Alert（有圖案）
    func showCustomAlert(mode: ButtonMode, iconName: String = "", message: String, yesBtnTitle: String = "是", noBtnTitle: String = "否", confirmTitle: String = "確認", yesBtnAction: @escaping () -> Void = {}, noBtnAction: @escaping () -> Void = {}, confirmBtnAction: @escaping () -> Void = {}) {
        let storyboard = UIStoryboard(name: "Alert", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        
        switch mode{
        case .One:
            controller.mode = true
        case .Two:
            controller.mode = false
        }
        
        controller.iconName = iconName
        controller.message = message
        controller.yesButtonTitle = yesBtnTitle
        controller.noButtonTitle = noBtnTitle
        controller.confirmButtonTitle = confirmTitle
        controller.yesButtonAction = yesBtnAction
        controller.noButtonAction = noBtnAction
        controller.confirmButtonAction = confirmBtnAction
        
        self.present(controller, animated: true, completion: nil)
    }

    ///客製化Alert（無按鈕）
    func showCustomNoBtnAlert(iconName: String = "success", message: String = "設定成功", cancelAcction: @escaping () -> Void = {}) {
        let storyboard = UIStoryboard(name: "Alert", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CustomNoBtnAlertViewController") as! CustomNoBtnAlertViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.iconName = iconName
        controller.message = message
        controller.cancelAcction = cancelAcction
        self.present(controller, animated: true, completion: nil)
    }
    
    ///新增球員Alert
    func showAddPlayerAlert(teamId: String, players: [Players], yesButtonAction: @escaping () -> Void) {
        let storyboard = UIStoryboard(name: "Alert", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "AddPlayerViewController") as! AddPlayerViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.teamId = teamId
        controller.players = players
        controller.yesButtonAction = yesButtonAction
        self.present(controller, animated: true, completion: nil)
    }
    
    ///編輯球員Alert
    func showEditPlayerAlert(editPlayer: Players, teamId: String, players: [Players], yesButtonAction: @escaping () -> Void) {
        let storyboard = UIStoryboard(name: "Alert", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "AddPlayerViewController") as! AddPlayerViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.isAdd = false
        controller.editPlayer = editPlayer
        controller.teamId = teamId
        controller.players = players
        controller.yesButtonAction = yesButtonAction
        self.present(controller, animated: true, completion: nil)
    }
    
    ///頭像選擇Alert
    func showPhotoSelectView(photosSelectAcction: @escaping (String) -> Void) {
        let storyboard = UIStoryboard(name: "Alert", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "PhotoSelectViewController") as! PhotoSelectViewController
//        controller.modalPresentationStyle = .automatic
//        controller.modalTransitionStyle = .crossDissolve
        controller.photosSelectAcction = photosSelectAcction
        self.present(controller, animated: true, completion: nil)
    }
    
    ///球隊選擇Alert
    func showTeamsSelectView(teamsSelectAcction: @escaping (Teams, Int) -> Void) {
        let storyboard = UIStoryboard(name: "Alert", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "TeamsSelectViewController") as! TeamsSelectViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.teamsSelectAcction = teamsSelectAcction
        self.present(controller, animated: true, completion: nil)
    }
    
    ///計分Alert
    func showScoringView(player: Players? = nil, eventAction: @escaping (Int) -> Void) {
        let storyboard = UIStoryboard(name: "Alert", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ScoringAlertViewController") as! ScoringAlertViewController
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.player = player
        controller.eventAction = eventAction
        self.present(controller, animated: false, completion: nil)
    }
}
